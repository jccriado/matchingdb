This directory stores a collection of MatchingDB dictionaries. The current ones are:
- **smeft_dim6_tree.json**
  - Description: The complete tree-level dictionary between the SMEFT with operators up to dimension 6 and its extensions with new particles. Given in the Warsaw basis, using wcxf notation for the coefficients.
  - Paper: [1711.10391](https://arxiv.org/abs/1711.10391). 
  - Generated with: [MatchingTools](github.com/jccriado/matchingtools). 
