import importlib

import jsonref
import jsonschema

schema = jsonref.loads(
    importlib.resources.files("matchingdb").joinpath("matchingdb.json").read_text()
)


validator = jsonschema.validators.extend(
    jsonschema.Draft202012Validator,
    type_checker=jsonschema.Draft202012Validator.TYPE_CHECKER.redefine(
        "array", lambda _, instance: isinstance(instance, (tuple, list))
    ),
)


def validate(instance):
    validator(schema=schema).validate(instance)

    for term in instance["terms"]:
        if sorted(term["fields"]) != term["fields"]:
            raise Exception(f"Unsorted fields in term: {term}")

    for coupling in instance["couplings"]:
        if sorted(coupling["fields"]) != coupling["fields"]:
            raise Exception(f"Unsorted fields in coupling: {coupling}")
