from collections import defaultdict


def merge_dictionaries(dictionaries, default_value, merge_function):
    out = defaultdict(lambda: default_value)

    for dictionary in dictionaries:
        for key, value in dictionary.items():
            out[key] = merge_function(out[key], value)

    return dict(out)
