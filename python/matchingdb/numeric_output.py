from collections import namedtuple

import numpy as np

from matchingdb.merge_dictionaries import merge_dictionaries


def mass(field):
    return f"M_{field}"


def evaluate_numerical_factor(factor, constant_values):
    number, exponent = factor
    return number ** exponent


def evaluate_constant_factor(factor, constant_values):
    name, exponent, _ = factor
    return constant_values[name] ** exponent


def evaluate_coupling_factor(factor, parameter_values):
    name, exponent, conjugated, _ = factor
    out = parameter_values[name] ** exponent
    if conjugated:
        out = out.conj()
    return out


def evaluate_mass_factor(factor, parameter_values):
    name, _, exponent = factor
    return parameter_values[mass(name)] ** exponent


def evaluate_mass_difference_factor(factor, parameter_values):
    minuend_name, _, subtrahend_name, _, exponent = factor
    minuend = parameter_values[mass(minuend_name)] ** 2
    subtrahend = parameter_values[mass(subtrahend_name)] ** 2
    return np.array(
        [[(min_ - sub) ** exponent for sub in subtrahend] for min_ in minuend]
    )


def evaluate_log_mass_factor(log, parameter_values):
    name, _, exponent = log
    return np.log((parameter_values[mass(name)] / parameter_values["mu"]) ** exponent)


def evaluate_log_mass_ratio_factor(log, parameter_values):
    num_name, _, den_name, _, exponent = log
    return np.log(
        (parameter_values[mass(num_name)] / parameter_values[mass(den_name)])
        ** exponent
    )


def evaluate_term(term, einsum_expr, parameter_values):
    factors = []

    for type_, *content in term["factors"]:
        if type_ == "numerical":
            factors.append(evaluate_numerical_factor(content, parameter_values))

        elif type_ == "constant":
            factors.append(evaluate_constant_factor(content, parameter_values))

        elif type_ == "coupling":
            factors.append(evaluate_coupling_factor(content, parameter_values))

        elif type_ == "mass":
            factors.append(evaluate_mass_factor(content, parameter_values))

        elif type_ == "mass_difference":
            factors.append(evaluate_mass_difference_factor(content, parameter_values))

        elif type_ == "log_mass":
            factors.append(evaluate_log_mass_factor(content, parameter_values))

        elif type_ == "log_mass_ratio":
            factors.append(evaluate_log_mass_ratio_factor(content, parameter_values))

    return {term["coefficient"]: np.einsum(einsum_expr, *factors)}


def evaluate_term_expanding_flavor(term, einsum_expr, parameter_values):
    """Evaluate a term and return a wcxf-like dict, with expanded flavor indices"""

    values = list(evaluate_term(term, einsum_expr, parameter_values).values())[0]

    def make_real(value):
        if np.isreal(value):
            return float(value)
        else:
            return {"Re": float(value.real), "Im": float(value.imag)}

    return {
        f"{term['coefficient']}_{''.join(map(str, indices))}": make_real(value)
        for indices, value in np.ndenumerate(values)
        if value != 0
    }


def vanishes(term, parameters):
    """Determine if a term vanishes, assuming the parameters not provided are zero"""

    for type_, *content in term["factors"]:
        if type_ in ["constant", "coupling"] and content[0] not in parameters:
            return True

        elif type_ == "mass" and mass(content[0]) not in parameters:
            return True

        elif type_ == "log_mass":
            if mass(content[0]) not in parameters or "mu" not in parameters:
                return True

        elif type_ in ["mass_difference", "log_mass_ratio"]:
            field1, _, field2, _, _ = content
            if field1 not in parameters or field2 not in parameters:
                return True

    return False


# The 52 characters that can be used as indices in np.einsum expressions
lower_letters = list(map(chr, range(ord("a"), ord("z") + 1)))
upper_letters = list(map(chr, range(ord("A"), ord("Z") + 1)))
all_letters = lower_letters + upper_letters


def prepare_einsum_expr(term):
    """Construct the np.einsum input expression for the index contractions of a term."""
    indices = []

    for type_, *content in term["factors"]:
        if type_ == "numerical":
            indices.append([])

        elif type_ in ["constant", "coupling"]:
            indices.append(content[-1])

        elif type_ in ["mass", "log_mass"]:
            indices.append([content[-1]])

        elif type_ in ["mass_difference", "log_mass_ratio"]:
            indices.append([content[1], content[3]])

    # List of all indices without redundancies
    in_indices = list({index for subindices in indices for index in subindices})

    # Mapping from multi-char indices into single-letter ones
    index_mapping = {
        in_index: out_index for in_index, out_index in zip(in_indices, all_letters)
    }
    indices = [[index_mapping[index] for index in subindices] for subindices in indices]
    free_indices = [index_mapping[index] for index in term["free_indices"]]

    # Final np.einsum expression, e.g. ",ij,ki,lmm->jl"
    return (
        ",".join("".join(map(str, subindices)) for subindices in indices)
        + "->"
        + "".join(map(str, free_indices))
    )


NumericalInput = namedtuple("NumericalInput", ["terms", "einsum_exprs"])
"""Structured representation for the input to `to_numeric`"""


def prepare_numerical_input(terms, parameters):
    """Remove vanishing terms and construct einsum expression."""
    terms = [term for term in terms if not vanishes(term, parameters)]
    einsum_exprs = [
        prepare_einsum_expr(term) for term in terms if not vanishes(term, parameters)
    ]
    return NumericalInput(terms=terms, einsum_exprs=einsum_exprs)


def make_array(value):
    if isinstance(value, dict):
        return np.array(value["Re"]) + 1j * np.array(value["Im"])
    return np.array(value)


def numeric_output_terms(terms, parameters, db):
    """Return a function that evaluates the terms for any values of the parameters."""

    constant_values = {
        constant["name"]: make_array(constant["value"])
        for constant in db.select_constants()
    }

    parameters = list(parameters) + list(constant_values)
    # Cache input for better performance in multiple calls to `out`
    cached = prepare_numerical_input(terms, parameters)

    def out(parameter_values, expand_flavor=False):
        """Evaluate the terms for the given values of the parameters"""
        evaluate = evaluate_term_expanding_flavor if expand_flavor else evaluate_term
        parameter_values = {**parameter_values, **constant_values}

        dicts = [
            evaluate(term, einsum_expr, parameter_values)
            for term, einsum_expr in zip(cached.terms, cached.einsum_exprs)
        ]

        return merge_dictionaries(dicts, 0, lambda x, y: x + y)

    return out
