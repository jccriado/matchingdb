from matchingdb.latex_output import latex_output_couplings, latex_output_terms
from matchingdb.numeric_output import numeric_output_terms
from matchingdb.pandas_output import (
    pandas_output_constants,
    pandas_output_couplings,
    pandas_output_fields,
    pandas_output_terms,
)
from matchingdb.validation import validate


def validate_options_string(name, string, options):
    if string not in options:
        raise ValueError(
            f"Invalid {name}: {string}"
            f"(must be one of: {', '.join(map(repr, options))})."
        )


def prepare_fields_select(fields, fields_criterion):
    if fields is None:
        return lambda _: True

    if fields_criterion == "equal":
        fields = sorted(fields)
        return lambda item_fields: item_fields == fields

    if fields_criterion == "subset":
        fields = set(fields)
        return lambda item_fields: set(item_fields) <= fields

    if fields_criterion == "supset":
        fields = set(fields)
        return lambda item_fields: set(item_fields) >= fields


def output_terms(terms, output_format, parameters, db):
    if output_format == "raw":
        return terms

    if output_format == "pandas":
        return pandas_output_terms(terms)

    if output_format == "numeric":
        return numeric_output_terms(terms, parameters, db)

    if output_format == "latex":
        return latex_output_terms(terms, db)


def output_couplings(couplings, output_format):
    if output_format == "raw":
        return couplings

    if output_format == "pandas":
        return pandas_output_couplings(couplings)

    if output_format == "latex":
        return latex_output_couplings(couplings)


def output_constants(constants, output_format):
    if output_format == "raw":
        return constants

    if output_format == "pandas":
        return pandas_output_constants(constants)


def output_fields(fields, output_format):
    if output_format == "raw":
        return fields

    if output_format == "pandas":
        return pandas_output_fields(fields)


def create_data(data):
    if data is None:
        return dict(fields=[], couplings=[], terms=[])

    validate(data)
    return data
