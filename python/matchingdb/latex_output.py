from matchingdb.merge_dictionaries import merge_dictionaries


def numerical_factor_latex(numerical_factor, db):
    number, exponent = numerical_factor
    exponent = abs(exponent)
    if number == 1 == exponent:
        return ""
    elif exponent == 1:
        if number < 0:
            return f"({number})"
        else:
            return str(number)
    elif exponent == 0.5:
        return rf"\sqrt{{{numerical_factor}}}"

    if number < 0:
        return f"({numerical_factor})^{{{exponent}}}"
    else:
        return f"{numerical_factor}^{{{exponent}}}"


def has_indices(name):
    return "^" in name or "_" in name


def constant_factor_latex(constant_factor, db):
    constant_name, exponent, indices = constant_factor
    exponent = abs(exponent)
    out = db.get_constant(constant_name)["latex"]

    if exponent == 1 and not indices:
        return out

    if has_indices(out):
        out = f"\\left({out}\\right)"
    else:
        out = f"{out}"

    if indices:
        if any(len(index) > 1 for index in indices):
            indices_str = ",".join(map(str, indices))
        else:
            indices_str = "".join(map(str, indices))
        out += f"_{{{indices_str}}}"

    if exponent == 1:
        return out

    exponent_str = str(exponent)
    return f"{out}^{{{exponent_str}}}"


def coupling_factor_latex(coupling_factor, db):
    coupling_name, exponent, conjugated, indices = coupling_factor
    exponent = abs(exponent)
    out = db.get_coupling(coupling_name)["latex"]

    if exponent == 1 and not conjugated and not indices:
        return out

    if has_indices(out):
        out = f"\\left({out}\\right)"
    else:
        out = f"{out}"

    if indices:
        if any(len(index) > 1 for index in indices):
            indices_str = ",".join(map(str, indices))
        else:
            indices_str = "".join(map(str, indices))
        out += f"_{{{indices_str}}}"

    if exponent == 1 and not conjugated:
        return out

    conjugated_str = "*" if conjugated else ""
    exponent_str = str(exponent) if exponent != 1 else ""
    return f"{out}^{{{conjugated_str}{exponent_str}}}"


def mass_factor_latex(mass_factor, db):
    field_name, index, exponent = mass_factor
    exponent = abs(exponent)
    field_latex = db.get_field(field_name)["latex"]
    mass_latex = f"M_{{{field_latex},{index}}}"
    if exponent == 1:
        return mass_latex
    return f"{mass_latex}^{{{exponent}}}"


def mass_difference_factor_latex(mass_difference_factor, db):
    min_name, min_index, sub_name, sub_index, exponent = mass_difference_factor
    exponent = abs(exponent)

    minuend = mass_factor_latex((min_name, min_index, 2), db)
    subtrahend = mass_factor_latex((sub_name, sub_index, 2), db)
    out = f"\\left({minuend} - {subtrahend}\\right)"

    if exponent == 1:
        return out

    return f"{out}^{{{exponent}}}"


def log_mass_factor_latex(log_mass_factor, db):
    field, index, exponent = log_mass_factor
    mass = mass_factor_latex((field, index, 1), db)

    if exponent > 0:
        base = rf"\frac{{{mass}}}{{\mu}}"
    else:
        base = rf"\frac{{\mu}}{{{mass}}}"

    if abs(exponent) == 1:
        arg = base
    else:
        arg = rf"\left({base}\right)^{{{abs(exponent)}}}"

    return rf"\log{{{arg}}}"


def log_mass_ratio_factor_latex(log_mass_ratio_factor, db):
    num_field, num_index, den_field, den_index, exponent = log_mass_ratio_factor
    num = mass_factor_latex((num_field, num_index, 1), db)
    den = mass_factor_latex((den_field, den_index, 1), db)

    if exponent < 0:
        exponent = -exponent
        num, den = den, num

    base = rf"\frac{{{num}}}{{{den}}}"

    if exponent == 1:
        arg = base
    else:
        arg = rf"\left({base}\right)^{{{exponent}}}"

    return rf"\log{{{arg}}}"


def exponent(factor_type, factor_content):
    if factor_type in ["constant", "coupling"]:
        return factor_content[1]
    return factor_content[-1]


def term_latex(term, db):
    numerator = ""
    denominator = ""
    log = ""

    for type_, *content in term["factors"]:
        if type_ == "numerical":
            factor_str = numerical_factor_latex(content, db)
        elif type_ == "constant":
            factor_str = constant_factor_latex(content, db)
        elif type_ == "coupling":
            factor_str = coupling_factor_latex(content, db)
        elif type_ == "mass":
            factor_str = mass_factor_latex(content, db)
        elif type_ == "mass_difference":
            factor_str = mass_difference_factor_latex(content, db)

        if type_ == "log_mass":
            log += log_mass_factor_latex(content, db)
        elif type_ == "log_mass_ratio":
            log += log_mass_ratio_factor_latex(content, db)
        else:
            if exponent(type_, content) > 0:
                numerator += " " + factor_str
            else:
                denominator += " " + factor_str

    if denominator == "":
        return f"{numerator} {log}"
    if numerator == "":
        numerator = "1"

    return {term["coefficient"]: f"\\frac{{{numerator}}}{{{denominator}}} {log}"}


def latex_output_terms(terms, db):
    dicts = [term_latex(term, db) for term in terms]
    return merge_dictionaries(dicts, "", lambda x, y: f"{x} + {y}")


def latex_output_couplings(couplings):
    return " + ".join(coupling["latex_interaction"] for coupling in couplings)
