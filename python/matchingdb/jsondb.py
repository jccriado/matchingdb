import json
import pathlib

from matchingdb.db_helpers import (
    create_data,
    output_constants,
    output_couplings,
    output_fields,
    output_terms,
    prepare_fields_select,
    validate_options_string,
)
from matchingdb.validation import schema, validator


class JsonDB:
    def __init__(self, filename, data):
        self.filename = filename
        self.data = data

    @staticmethod
    def new(filename, data=None):
        data = create_data(data)

    @staticmethod
    def load(filename):
        return JsonDB(filename, json.loads(pathlib.Path(filename).read_text()))

    def save(self):
        pathlib.Path(self.filename).write_text(json.dumps(self.data))

    def insert(self, item, table):
        validator(schema=schema["$defs"][table[:-1]]).validate(item)
        self.data[table].append(item)

    def select_terms(
        self,
        coefficient=None,
        fields=None,
        condition=None,
        output_format="raw",
        parameters=None,
        fields_criterion="equal",
    ):
        if condition is None:
            condition = lambda _: True

        validate_options_string(
            "output_format", output_format, ["raw", "pandas", "latex", "numeric"]
        )
        validate_options_string(
            "fields_criterion", fields_criterion, ["equal", "subset", "supset"]
        )

        fields_select = prepare_fields_select(fields, fields_criterion)
        terms = [
            term
            for term in self.data["terms"]
            if (coefficient is None or term["coefficient"] == coefficient)
            and fields_select(term["fields"])
            and condition(term)
        ]

        return output_terms(terms, output_format, parameters, self)

    def select_couplings(
        self,
        name=None,
        fields=None,
        output_format="raw",
        fields_criterion="equal",
    ):
        validate_options_string(
            "output_format", output_format, ["raw", "pandas", "latex"]
        )
        validate_options_string(
            "fields_criterion", fields_criterion, ["equal", "subset", "supset"]
        )

        fields_select = prepare_fields_select(fields, fields_criterion)
        couplings = [
            coupling
            for coupling in self.data["couplings"]
            if (name is None or coupling["name"] == name)
            and fields_select(coupling["fields"])
        ]

        return output_couplings(couplings, output_format)

    def select_constants(self, name=None, output_format="raw"):
        validate_options_string("output_format", output_format, ["raw", "pandas"])

        constants = [
            constant
            for constant in self.data["constants"]
            if name is None or constant["name"] == name
        ]

        return output_constants(constants, output_format)

    def select_fields(self, name=None, output_format="raw"):
        validate_options_string("output_format", output_format, ["raw", "pandas"])

        fields = [
            field
            for field in self.data["fields"]
            if name is None or field["name"] == name
        ]

        return output_fields(fields, output_format)

    def get_coupling(self, name):
        for coupling in self.data["couplings"]:
            if coupling["name"] == name:
                return coupling

        raise ValueError(f"Coupling not found: {name}")

    def get_constant(self, name):
        for constant in self.data["constants"]:
            if constant["name"] == name:
                return constant

        raise ValueError(f"Constant not found: {name}")

    def get_field(self, name):
        for field in self.data["fields"]:
            if field["name"] == name:
                return field

        raise ValueError(f"Field not found: {name}")
