import pandas as pd


def pandas_output_terms(terms):
    if not terms:
        return None

    df = pd.DataFrame(terms)
    df["couplings"] = df["factors"].apply(
        lambda x: [y[1] for y in x if y[0] == "coupling"]
    )
    return df[["coefficient", "fields", "couplings"]]


def pandas_output_constants(constants):
    if not constants:
        return None

    return pd.DataFrame(constants)[["name", "value", "latex"]]


def pandas_output_couplings(couplings):
    if not couplings:
        return None

    return pd.DataFrame(couplings)[["name", "fields", "real"]]


def pandas_output_fields(fields):
    if not fields:
        return None

    return pd.DataFrame(fields)[["name", "real", "representation"]]
