import json
import sqlite3

from matchingdb.db_helpers import (
    create_data,
    output_constants,
    output_couplings,
    output_fields,
    output_terms,
    prepare_fields_select,
    validate_options_string,
)
from matchingdb.validation import schema, validator

field_columns = schema["$defs"]["field"]["properties"]
coupling_columns = schema["$defs"]["coupling"]["properties"]
constant_columns = schema["$defs"]["constant"]["properties"]
term_columns = schema["$defs"]["term"]["properties"]


class SQLiteDB:
    def __init__(self, connection, cursor):
        self.connection = connection
        self.cursor = cursor

    @staticmethod
    def load(filename):
        connection = sqlite3.connect(filename)
        return SQLiteDB(connection, connection.cursor())

    @staticmethod
    def new(filename, data=None):
        data = create_data(data)

        out = SQLiteDB.load(filename)
        out._create_tables()

        for field in data["fields"]:
            out.insert(field, "fields")

        for coupling in data["couplings"]:
            out.insert(coupling, "couplings")

        for constant in data["constants"]:
            out.insert(constant, "constants")

        for term in data["terms"]:
            out.insert(term, "terms")

        return out

    def _create_table(self, name, columns):
        definition = ", ".join(f"{column} STRING" for column in columns)
        self.cursor.execute(f"CREATE TABLE {name} ({definition})")

    def _create_tables(self):
        self._create_table("fields", field_columns)
        self._create_table("couplings", coupling_columns)
        self._create_table("constants", constant_columns)
        self._create_table("terms", term_columns)

    def save(self):
        self.connection.commit()

    def insert(self, item, table):
        validator(schema=schema["$defs"][table[:-1]]).validate(item)

        row = list(map(dumps, item.values()))
        qmarks = ", ".join("?" * len(row))
        self.cursor.execute(f"INSERT INTO {table} VALUES ({qmarks})", row)

    def select_terms(
        self,
        coefficient=None,
        fields=None,
        condition=None,
        output_format="raw",
        parameters=None,
        fields_criterion="equal",
    ):
        validate_options_string(
            "output_format", output_format, ["raw", "pandas", "numeric", "latex"]
        )
        validate_options_string(
            "fields_criterion", fields_criterion, ["equal", "subset", "supset"]
        )

        if fields is not None and fields_criterion == "equal":
            fields_equal = sorted(fields)
        else:
            fields_equal = None

        if coefficient is None and fields_equal is None:
            query = "SELECT * FROM terms"
            filters = ()
        elif coefficient is not None and fields_equal is None:
            query = "SELECT * FROM terms WHERE coefficient=?"
            filters = (dumps(coefficient),)
        elif coefficient is None and fields_equal is not None:
            query = "SELECT * FROM terms WHERE fields=?"
            filters = (dumps(fields_equal),)
        else:
            query = "SELECT * FROM terms WHERE coefficient=? AND fields=?"
            filters = (dumps(coefficient), dumps(fields_equal))

        rows = self.cursor.execute(query, filters).fetchall()
        terms = list(map(make_term, rows))

        if fields_criterion in ["subset", "supset"]:
            fields_select = prepare_fields_select(fields, fields_criterion)
            terms = [term for term in terms if fields_select(term["fields"])]

        if condition is not None:
            terms = [term for term in terms if condition(term)]

        return output_terms(terms, output_format, parameters, self)

    def select_couplings(
        self, name=None, fields=None, output_format="raw", fields_criterion="equal"
    ):
        validate_options_string(
            "output_format", output_format, ["raw", "pandas", "latex"]
        )
        validate_options_string(
            "fields_criterion", fields_criterion, ["equal", "subset", "supset"]
        )

        if fields is not None and fields_criterion == "equal":
            fields_equal = sorted(fields)
        else:
            fields_equal = None

        if name is None and fields_equal is None:
            query = "SELECT * FROM couplings"
            filters = ()
        elif name is not None and fields_equal is None:
            query = "SELECT * FROM couplings WHERE name=?"
            filters = (dumps(name),)
        elif name is None and fields_equal is not None:
            query = "SELECT * FROM couplings WHERE fields=?"
            filters = (dumps(fields_equal),)
        else:
            query = "SELECT * FROM couplings WHERE name=? AND fields=?"
            filters = (dumps(name), dumps(fields_equal))

        rows = self.cursor.execute(query, filters).fetchall()
        couplings = list(map(make_coupling, rows))

        if fields_criterion in ["subset", "supset"]:
            fields_select = prepare_fields_select(fields, fields_criterion)
            couplings = [
                coupling for coupling in couplings if fields_select(coupling["fields"])
            ]

        return output_couplings(couplings, output_format)

    def select_constants(self, name=None, output_format="raw"):
        validate_options_string("output_format", output_format, ["raw", "pandas"])

        if name is None:
            query = "SELECT * FROM constants"
            filters = ()
        else:
            query = "SELECT * FROM constants WHERE name=?"
            filters = (dumps(name),)

        rows = self.cursor.execute(query, filters).fetchall()
        constants = list(map(make_constant, rows))

        return output_constants(constants, output_format)

    def select_fields(self, name=None, output_format="raw"):
        validate_options_string("output_format", output_format, ["raw", "pandas"])

        if name is None:
            query = "SELECT * FROM fields"
            filters = ()
        else:
            query = "SELECT * FROM fields WHERE name=?"
            filters = (dumps(name),)

        rows = self.cursor.execute(query, filters).fetchall()
        fields = list(map(make_field, rows))

        return output_fields(fields, output_format)

    def get_coupling(self, name):
        query = "SELECT * FROM couplings WHERE name=?"
        filters = (dumps(name),)

        row = self.cursor.execute(query, filters).fetchone()
        return make_coupling(row)

    def get_constant(self, name):
        query = "SELECT * FROM constants WHERE name=?"
        filters = (dumps(name),)

        row = self.cursor.execute(query, filters).fetchone()
        return make_constant(row)

    def get_field(self, name):
        query = "SELECT * FROM fields WHERE name=?"
        filters = (dumps(name),)

        row = self.cursor.execute(query, filters).fetchone()
        return make_field(row)


def dumps(item):
    return json.dumps(item, separators=(",", ":"))


def make_term(row):
    return dict(zip(term_columns, map(json.loads, row)))


def make_coupling(row):
    return dict(zip(coupling_columns, map(json.loads, row)))


def make_constant(row):
    return dict(zip(constant_columns, map(json.loads, row)))


def make_field(row):
    return dict(zip(field_columns, map(json.loads, row)))
