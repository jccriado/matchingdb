"""
SMEFT leptons loop-generated terms example

Creating an SQLite database for some terms, viewing them and inserting new ones
"""

import pathlib
from math import pi

from matchingdb import SQLiteDB

# Define the terms directly in Python code
terms = [
    dict(
        coefficient="phie",
        fields=["E"],
        factors=[
            ["numerical", 2, 1],
            ["numerical", 16 * 15, -1],
            ["constant", "pi", -2, []],
            ["constant", "kdelta", 1, ["i", "j"]],
            ["coupling", "g1", 4, False, []],
            ["mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phie",
        fields=["E"],
        factors=[
            ["numerical", -13, 1],
            ["numerical", 16 * 36, -1],
            ["constant", "pi", -2, []],
            ["constant", "kdelta", 1, ["i", "j"]],
            ["coupling", "g1", 2, False, []],
            ["coupling", "lambda_tilde", 1, False, ["k", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["k", "a"]],
            ["mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phie",
        fields=["E"],
        factors=[
            ["numerical", 16 * 24, -1],
            ["constant", "pi", -2, []],
            ["coupling", "Ye", 1, True, ["k", "i"]],
            ["coupling", "lambda_tilde", 1, False, ["k", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["l", "a"]],
            ["coupling", "Ye", 1, False, ["l", "j"]],
            ["mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phie",
        fields=["E"],
        factors=[
            ["numerical", -16 * 6, -1],
            ["constant", "pi", -2, []],
            ["constant", "kdelta", 1, ["i", "j"]],
            ["coupling", "g1", 2, False, []],
            ["coupling", "lambda_tilde", 1, False, ["k", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["k", "a"]],
            ["mass", "E", "a", -2],
            ["log_mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phie",
        fields=["E"],
        factors=[
            ["numerical", 16 * 4, -1],
            ["constant", "pi", -2, []],
            ["coupling", "Ye", 1, True, ["k", "i"]],
            ["coupling", "lambda_tilde", 1, False, ["k", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["l", "a"]],
            ["coupling", "Ye", 1, False, ["l", "j"]],
            ["mass", "E", "a", -2],
            ["log_mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phil3",
        fields=["E"],
        factors=[
            ["numerical", -4, -1],
            ["coupling", "lambda_tilde", 1, False, ["i", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["j", "a"]],
            ["mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phil3",
        fields=["E"],
        factors=[
            ["numerical", -5, 1],
            ["numerical", 16 * 72, -1],
            ["constant", "pi", -2, []],
            ["constant", "kdelta", 1, ["i", "j"]],
            ["coupling", "g2", 2, False, []],
            ["coupling", "lambda_tilde", 1, False, ["k", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["k", "a"]],
            ["mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
    dict(
        coefficient="phil3",
        fields=["E"],
        factors=[
            ["numerical", -16, -1],
            ["constant", "pi", -2, []],
            ["constant", "kdelta", 1, ["i", "j"]],
            ["coupling", "g2", 2, False, []],
            ["coupling", "lambda_tilde", 1, False, ["k", "a"]],
            ["coupling", "lambda_tilde", 1, True, ["k", "a"]],
            ["mass", "E", "a", -2],
            ["log_mass", "E", "a", -2],
        ],
        free_indices=["i", "j"],
    ),
]

fields = [dict(name="E", real=False, representation="F(1, 1, -1)", latex="E")]

couplings = [
    dict(
        name="lambda_tilde",
        fields=["E"],
        real=False,
        latex=r"\tilde{\lambda}",
        latex_interaction="",
    ),
    dict(name="g1", fields=[], real=True, latex=r"g_1", latex_interaction=""),
    dict(name="g2", fields=[], real=True, latex=r"g_2", latex_interaction=""),
    dict(name="Ye", fields=[], real=False, latex=r"Y_e", latex_interaction=""),
]

constants = [
    dict(name="pi", value=pi, latex=r"\pi"),
    dict(name="kdelta", value=[[1, 0, 0], [0, 1, 0], [0, 0, 1]], latex=r"\delta"),
]

lepton_loop_data = dict(
    fields=fields, couplings=couplings, constants=constants, terms=terms
)

# Clear existing files
if pathlib.Path("leptons.sqlite").exists():
    pathlib.Path("leptons.sqlite").unlink()

# Create a sqlite database with all terms except the last one
db = SQLiteDB.new("leptons.sqlite", data=lepton_loop_data)


print(db.select_terms(output_format="pandas"))

db.insert(terms[-1], "terms")

print(db.select_terms(output_format="pandas"))
