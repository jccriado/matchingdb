import numpy as np
import smelli

from matchingdb import JsonDB

gl = smelli.GlobalLikelihood()
db = JsonDB.load("smeft_dim6_tree.json")

evaluator = db.select_terms(
    fields=["B"], output_format="numeric", parameters={"gphiB", "M_B"}
)

coeff_values = evaluator({"gphiB": np.array([0.3]), "M_B": np.array([2000.0])})
pp = gl.parameter_point(coeff_values, scale=1000)
df = pp.obstable()
print(df.sort_values("pull SM", ascending=True))
