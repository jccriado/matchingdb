import numpy as np

from matchingdb.jsondb import JsonDB

tree6_db = JsonDB.load("examples/smeft_dim6_tree.json")


def test_select_terms_raw_tree6_L1Xi():
    assert tree6_db.select_terms(
        coefficient="phiD", fields=["L1", "Xi"], output_format="raw"
    ) == [
        {
            "coefficient": "phiD",
            "fields": ["L1", "Xi"],
            "factors": [
                ["numerical", 2, 1],
                ["numerical", 1, -1],
                ["coupling", "epsilonXiL1", 1, False, ["b", "c", "a"]],
                ["coupling", "kappaXi", 1, False, ["b"]],
                ["coupling", "gammaL1", 1, True, ["c"]],
                ["coupling", "gammaL1", 1, False, ["a"]],
                ["mass", "Xi", "b", -2],
                ["mass", "L1", "c", -2],
                ["mass", "L1", "a", -2],
            ],
            "free_indices": [],
        },
        {
            "coefficient": "phiD",
            "fields": ["L1", "Xi"],
            "factors": [
                ["numerical", 2, 1],
                ["numerical", 1, -1],
                ["coupling", "gPrimeXiL1", 1, False, ["a", "b"]],
                ["coupling", "kappaXi", 1, False, ["a"]],
                ["coupling", "gammaL1", 1, False, ["b"]],
                ["mass", "Xi", "a", -2],
                ["mass", "L1", "b", -2],
            ],
            "free_indices": [],
        },
        {
            "coefficient": "phiD",
            "fields": ["L1", "Xi"],
            "factors": [
                ["numerical", 2, 1],
                ["numerical", 1, -1],
                ["coupling", "gPrimeXiL1", 1, True, ["a", "b"]],
                ["coupling", "gammaL1", 1, True, ["b"]],
                ["coupling", "kappaXi", 1, False, ["a"]],
                ["mass", "L1", "b", -2],
                ["mass", "Xi", "a", -2],
            ],
            "free_indices": [],
        },
        {
            "coefficient": "phiD",
            "fields": ["L1", "Xi"],
            "factors": [
                ["numerical", -2, 1],
                ["numerical", 1, -1],
                ["coupling", "gXiL1", 1, False, ["a", "b"]],
                ["coupling", "kappaXi", 1, False, ["a"]],
                ["coupling", "gammaL1", 1, False, ["b"]],
                ["mass", "Xi", "a", -2],
                ["mass", "L1", "b", -2],
            ],
            "free_indices": [],
        },
        {
            "coefficient": "phiD",
            "fields": ["L1", "Xi"],
            "factors": [
                ["numerical", -2, 1],
                ["numerical", 1, -1],
                ["coupling", "gXiL1", 1, True, ["a", "b"]],
                ["coupling", "gammaL1", 1, True, ["b"]],
                ["coupling", "kappaXi", 1, False, ["a"]],
                ["mass", "L1", "b", -2],
                ["mass", "Xi", "a", -2],
            ],
            "free_indices": [],
        },
    ]


def test_select_terms_raw_tree6_Qcal1():
    assert tree6_db.select_terms(fields=["Qcal1"], output_format="raw") == [
        {
            "coefficient": "qd1",
            "fields": ["Qcal1"],
            "factors": [
                ["numerical", 2, 1],
                ["numerical", 3, -1],
                ["coupling", "gdqQcal1", 1, True, ["a", "l", "j"]],
                ["coupling", "gdqQcal1", 1, False, ["a", "k", "i"]],
                ["mass", "Qcal1", "a", -2],
            ],
            "free_indices": ["i", "j", "k", "l"],
        },
        {
            "coefficient": "qd8",
            "fields": ["Qcal1"],
            "factors": [
                ["numerical", -2, 1],
                ["numerical", 1, -1],
                ["coupling", "gdqQcal1", 1, True, ["a", "l", "j"]],
                ["coupling", "gdqQcal1", 1, False, ["a", "k", "i"]],
                ["mass", "Qcal1", "a", -2],
            ],
            "free_indices": ["i", "j", "k", "l"],
        },
        {
            "coefficient": "duql",
            "fields": ["Qcal1"],
            "factors": [
                ["numerical", 2, 1],
                ["numerical", 1, -1],
                ["coupling", "gdqQcal1", 1, True, ["a", "i", "k"]],
                ["coupling", "gulQcal1", 1, False, ["a", "j", "l"]],
                ["mass", "Qcal1", "a", -2],
            ],
            "free_indices": ["i", "j", "k", "l"],
        },
        {
            "coefficient": "lu",
            "fields": ["Qcal1"],
            "factors": [
                ["numerical", 1, 1],
                ["numerical", 1, -1],
                ["coupling", "gulQcal1", 1, True, ["a", "k", "i"]],
                ["coupling", "gulQcal1", 1, False, ["a", "l", "j"]],
                ["mass", "Qcal1", "a", -2],
            ],
            "free_indices": ["i", "j", "k", "l"],
        },
    ]


def test_select_couplings_raw_tree6_Xi1():
    assert tree6_db.select_couplings(fields=["Xi1"], output_format="raw") == [
        {
            "name": "yXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "y_{\\Xi_1}",
            "latex_interaction": "(y_{\\Xi_1})_{rij} \\Xi^{a\\dagger}_{1r} \\bar{l}_{Li}"
            " \\sigma^a i\\sigma_2 l^c_{Lj}",
        },
        {
            "name": "kappaXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "\\kappa_{\\Xi_1}",
            "latex_interaction": "(\\kappa_{\\Xi_1})_r \\Xi_{1r}^{a\\dagger} \\left("
            "\\tilde{\\phi}^\\dagger \\sigma^a \\phi\\right)",
        },
        {
            "name": "lambdaXi1",
            "fields": ["Xi1"],
            "real": True,
            "latex": "\\lambda_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{2} (\\lambda_{\\Xi_1})_{rs} \\left("
            "\\Xi^{a\\dagger}_{1r} \\Xi^a_{1s}\\right) "
            "\\left(\\phi^\\dagger\\phi\\right)",
        },
        {
            "name": "lambdaPrimeXi1",
            "fields": ["Xi1"],
            "real": True,
            "latex": "\\lambda'_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{2} (\\lambda'_{\\Xi_1})_{rs} f_{abc}"
            " \\left(\\Xi_{1r}^{a\\dagger} \\Xi_{1s}^b\\right) "
            "\\left(\\phi^\\dagger \\sigma^c \\phi\\right)",
        },
        {
            "name": "kphiXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "\\tilde{k}_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{f} (\\tilde{k}_{\\Xi_1})_r "
            "\\Xi^{a\\dagger}_{1r} D_\\mu \\tilde{\\phi}^\\dagger "
            "\\sigma^a D^\\mu \\phi",
        },
        {
            "name": "lambdaTildeXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "\\tilde{\\lambda}_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{f} (\\tilde{\\lambda}_{\\Xi_1})_r "
            "\\Xi^{a\\dagger}_{1r} |\\phi|^2 \\tilde{\\phi}^\\dagger \\sigma^a \\phi",
        },
        {
            "name": "yeXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "\\tilde{y}^e_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{f} (\\tilde{y}^e_{\\Xi_1})_{rij} "
            "\\Xi^{a\\dagger}_{1r} \\bar{e}_{Ri} \\tilde{\\phi}^\\dagger "
            "\\sigma^a l_{Lj}",
        },
        {
            "name": "ydXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "\\tilde{y}^d_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{f} (\\tilde{y}^d_{\\Xi_1})_{rij} "
            "\\Xi^{a\\dagger}_{1r} \\bar{d}_{Ri} \\tilde{\\phi}^\\dagger "
            "\\sigma^a q_{Lj}",
        },
        {
            "name": "yuXi1",
            "fields": ["Xi1"],
            "real": False,
            "latex": "\\tilde{y}^u_{\\Xi_1}",
            "latex_interaction": "\\frac{1}{f} (\\tilde{y}^u_{\\Xi_1})_{rij} "
            "\\Xi^{a\\dagger}_{1r} \\bar{q}_{Li} \\sigma^a \\phi u_{Rj}",
        },
    ]


def test_select_fields_raw_tree6_T2():
    assert tree6_db.select_fields(name="T2", output_format="raw") == [
        {"name": "T2", "real": False, "representation": "F(3,3,2/3)", "latex": "T_2"}
    ]


def test_select_terms_numeric_tree6_L3():
    np.random.seed(0)
    evaluator = tree6_db.select_terms(
        fields=["L3"], parameters={"gL3", "M_L3"}, output_format="numeric"
    )
    output = evaluator(
        {"gL3": np.random.random(size=(3, 3, 2)), "M_L3": np.random.random(size=(2,))}
    )
    assert list(output.keys()) == ["le"]
    assert np.isclose(
        output["le"],
        np.array(
            [
                [
                    [
                        [2.423714035502271, 2.3567991042864147, 1.7551413596999734],
                        [2.3567991042864147, 3.8554999746257956, 3.0313189441176607],
                        [1.7551413596999734, 3.0313189441176607, 2.3980523991491673],
                    ],
                    [
                        [3.8896801985658214, 1.534814251110128, 3.1476141305985124],
                        [4.031457866908337, 2.093101663507895, 2.8481712958884735],
                        [3.0550721419366087, 1.5938575680938383, 2.1081919396386954],
                    ],
                ],
                [
                    [
                        [3.8896801985658214, 4.031457866908337, 3.0550721419366087],
                        [1.534814251110128, 2.093101663507895, 1.5938575680938383],
                        [3.1476141305985124, 2.8481712958884735, 2.1081919396386954],
                    ],
                    [
                        [6.431157729596672, 2.4145899173290677, 5.066074287617777],
                        [2.4145899173290677, 1.3421751117379237, 1.8646562028508211],
                        [5.066074287617777, 1.8646562028508211, 4.132394415134343],
                    ],
                ],
            ]
        ),
    ).all()


def test_select_terms_numeric_expand_flavor_tree6_L3():
    np.random.seed(0)
    evaluator = tree6_db.select_terms(
        fields=["L3"], parameters={"gL3", "M_L3"}, output_format="numeric"
    )
    output = evaluator(
        {
            "gL3": np.random.random(size=(3, 3, 2)),
            "M_L3": np.random.random(size=(2,)),
        },
        expand_flavor=True,
    )
    for key, value in {
        "le_0000": 2.423714035502271,
        "le_0001": 2.3567991042864147,
        "le_0002": 1.7551413596999734,
        "le_0010": 2.3567991042864147,
        "le_0011": 3.8554999746257956,
        "le_0012": 3.0313189441176607,
        "le_0020": 1.7551413596999734,
        "le_0021": 3.0313189441176607,
        "le_0022": 2.3980523991491673,
        "le_0100": 3.8896801985658214,
        "le_0101": 1.534814251110128,
        "le_0102": 3.1476141305985124,
        "le_0110": 4.031457866908337,
        "le_0111": 2.093101663507895,
        "le_0112": 2.8481712958884735,
        "le_0120": 3.0550721419366087,
        "le_0121": 1.5938575680938383,
        "le_0122": 2.1081919396386954,
        "le_1000": 3.8896801985658214,
        "le_1001": 4.031457866908337,
        "le_1002": 3.0550721419366087,
        "le_1010": 1.534814251110128,
        "le_1011": 2.093101663507895,
        "le_1012": 1.5938575680938383,
        "le_1020": 3.1476141305985124,
        "le_1021": 2.8481712958884735,
        "le_1022": 2.1081919396386954,
        "le_1100": 6.431157729596672,
        "le_1101": 2.4145899173290677,
        "le_1102": 5.066074287617777,
        "le_1110": 2.4145899173290677,
        "le_1111": 1.3421751117379237,
        "le_1112": 1.8646562028508211,
        "le_1120": 5.066074287617777,
        "le_1121": 1.8646562028508211,
        "le_1122": 4.132394415134343,
    }.items():
        assert np.isclose(output[key], value)


def test_select_terms_latex_tree6_Delta1():
    assert (
        tree6_db.select_terms(fields=["Delta1"], output_format="latex")["eB"]
        == r" + \frac{  \left(\tilde{\lambda}^B_{\Delta_1}\right)_{ai}^{*} "
        r"\left(\lambda_{\Delta_1}\right)_{aj}}{  f M_{\Delta_1,a}} "
    )


def test_select_terms_latex_tree6_S():
    assert (
        tree6_db.select_terms(fields=["S"], output_format="latex")["phiG"]
        == r" + \frac{  \left(\tilde{k}^G_{\mathcal{S}}\right)_{a} \left(\kappa_{"
        r"\mathcal{S}}\right)_{a}}{  f M_{\mathcal{S},a}^{2}} "
    )


def test_select_couplings_latex_tree6_S():
    assert tree6_db.select_couplings(fields=["Y1"], output_format="latex") == (
        r"\frac{1}{2} (g_{\mathcal{Y}_1})_{rij} \mathcal{Y}^{AB\mu\dagger}_{1r}"
        r" \bar{d}^{(A|}_{Ri} \gamma_\mu i\sigma_2 q^{c|B)}_{Lj}"
    )


def test_insert_term():
    db = JsonDB("temp.json", dict(fields=[], couplings=[], terms=[]))
    db.insert(tree6_db.data["terms"][0], "terms")
    assert db.data["terms"] == [tree6_db.data["terms"][0]]


def test_select_terms_supset_tree6_S():
    terms = tree6_db.select_terms(
        fields=["S"], output_format="raw", fields_criterion="supset"
    )
    assert [term["fields"] for term in terms] == [
        ["S", "varphi"],
        ["L1", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["D", "S"],
        ["Q1", "S"],
        ["S"],
        ["S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["L1", "S"],
        ["L1", "S", "varphi"],
        ["L1", "S"],
        ["S", "varphi"],
        ["L1", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["E", "S"],
        ["Delta1", "S"],
        ["S"],
        ["S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["L1", "S"],
        ["L1", "S", "varphi"],
        ["L1", "S"],
        ["S"],
        ["S"],
        ["S", "varphi"],
        ["S", "varphi"],
        ["S", "Xi", "varphi"],
        ["S", "Xi1", "varphi"],
        ["S", "varphi"],
        ["S", "Xi", "varphi"],
        ["S", "Xi1", "varphi"],
        ["S", "Xi"],
        ["S", "Xi"],
        ["S", "Xi1"],
        ["S", "Xi1"],
        ["S", "Xi1"],
        ["L1", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["S"],
        ["S"],
        ["L1", "S", "varphi"],
        ["L1", "S", "varphi"],
        ["S"],
        ["L1", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["S"],
        ["L1", "S", "varphi"],
        ["L1", "S", "varphi"],
        ["S"],
        ["S"],
        ["S"],
        ["S"],
        ["S"],
        ["S"],
        ["S"],
        ["L1", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["S", "varphi"],
        ["L1", "S"],
        ["L1", "S"],
        ["L1", "S"],
        ["S", "U"],
        ["Q1", "S"],
        ["S"],
        ["S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["B", "S"],
        ["L1", "S", "varphi"],
        ["L1", "S"],
        ["L1", "S"],
    ]


def test_select_terms_subset_tree6_Delta1Delta3():
    terms = tree6_db.select_terms(
        fields=["Delta1", "Delta3"], output_format="raw", fields_criterion="subset"
    )
    assert [term["fields"] for term in terms] == [
        ["Delta1"],
        ["Delta1"],
        ["Delta1"],
        ["Delta1"],
        ["Delta1"],
        ["Delta1"],
        ["Delta1"],
        ["Delta3"],
        ["Delta3"],
        ["Delta3"],
        ["Delta3"],
        ["Delta1"],
        ["Delta1"],
        ["Delta1"],
        ["Delta3"],
        ["Delta3"],
        ["Delta3"],
    ]


def test_select_couplings_subset_tree6_Delta1Delta3():
    couplings = tree6_db.select_couplings(
        fields=["Delta1", "Delta3"], output_format="raw", fields_criterion="subset"
    )
    assert [coupling["name"] for coupling in couplings] == [
        "lambdaDelta1",
        "lambdaDelta3",
        "lambdaTildeeDelta3",
        "lambdaTildelDelta3",
        "lambdaTildeeDelta1",
        "lambdaTildeBDelta1",
        "lambdaTildeWDelta1",
        "lambdaTildelDelta1",
        "lambdaTildePrimelDelta1",
        "f",
        "Ye",
        "Yd",
        "Yu",
        "g1",
        "g2",
        "lambdaphi",
        "mu2phi",
    ]
