import pathlib

import numpy as np

from matchingdb.jsondb import JsonDB
from matchingdb.sqlitedb import SQLiteDB

tree6_jsondb = JsonDB.load("examples/smeft_dim6_tree.json")
sqlite_path = pathlib.Path("tests/smeft_dim6_tree.sqlite")

if sqlite_path.exists():
    sqlite_path.unlink()

tree6_sqlitedb = SQLiteDB.new("tests/smeft_dim6_tree.sqlite", data=tree6_jsondb.data)


def test_select_terms_raw_tree6_L1Xi():
    assert tree6_jsondb.select_terms(
        coefficient="phiD", fields=["L1", "Xi"], output_format="raw"
    ) == tree6_sqlitedb.select_terms(
        coefficient="phiD", fields=["L1", "Xi"], output_format="raw"
    )


def test_select_terms_raw_tree6_Qcal1():
    assert tree6_jsondb.select_terms(
        fields=["Qcal1"], output_format="raw"
    ) == tree6_sqlitedb.select_terms(fields=["Qcal1"], output_format="raw")


def test_select_couplings_raw_tree6_Xi1():
    assert tree6_jsondb.select_couplings(
        fields=["Xi1"], output_format="raw"
    ) == tree6_sqlitedb.select_couplings(fields=["Xi1"], output_format="raw")


def test_select_fields_raw_tree6_T2():
    assert tree6_jsondb.select_fields(
        name="T2", output_format="raw"
    ) == tree6_sqlitedb.select_fields(name="T2", output_format="raw")


def test_select_terms_numeric_tree6_L3():
    evaluator_json = tree6_jsondb.select_terms(
        fields=["L3"], parameters={"gL3", "M_L3"}, output_format="numeric"
    )
    evaluator_sqlite = tree6_sqlitedb.select_terms(
        fields=["L3"], parameters={"gL3", "M_L3"}, output_format="numeric"
    )
    parameter_values = {
        "gL3": np.random.random(size=(3, 3, 2)),
        "M_L3": np.random.random(size=(2,)),
    }
    output_json = evaluator_json(parameter_values)
    output_sqlite = evaluator_sqlite(parameter_values)
    assert list(output_json.keys()) == list(output_sqlite.keys())
    assert np.isclose(output_json["le"], output_sqlite["le"]).all()


def test_select_terms_numeric_expand_flavor_tree6_L3():
    evaluator_json = tree6_jsondb.select_terms(
        fields=["L3"], parameters={"gL3", "M_L3"}, output_format="numeric"
    )
    parameter_values = {
        "gL3": np.random.random(size=(3, 3, 2)),
        "M_L3": np.random.random(size=(2,)),
    }
    output_json = evaluator_json(
        parameter_values,
        expand_flavor=True,
    )
    evaluator_sqlite = tree6_sqlitedb.select_terms(
        fields=["L3"], parameters={"gL3", "M_L3"}, output_format="numeric"
    )
    output_sqlite = evaluator_sqlite(
        parameter_values,
        expand_flavor=True,
    )
    for key, value in output_json.items():
        assert np.isclose(output_sqlite[key], value)


def test_select_terms_latex_tree6_Delta1():
    assert tree6_jsondb.select_terms(
        fields=["Delta1"], output_format="latex"
    ) == tree6_sqlitedb.select_terms(fields=["Delta1"], output_format="latex")


def test_select_terms_latex_tree6_S():
    assert tree6_jsondb.select_terms(
        fields=["S"], output_format="latex"
    ) == tree6_sqlitedb.select_terms(fields=["S"], output_format="latex")


def test_insert_term():
    path = pathlib.Path("tests/simple.sqlite")
    if path.exists():
        path.unlink()

    db = SQLiteDB.new(
        "tests/simple.sqlite", dict(fields=[], couplings=[], terms=[], constants=[])
    )
    db.insert(tree6_jsondb.data["terms"][0], "terms")
    assert db.select_terms() == [tree6_jsondb.data["terms"][0]]


def test_select_couplings_latex_tree6_S():
    assert tree6_jsondb.select_couplings(
        fields=["Y1"], output_format="latex"
    ) == tree6_sqlitedb.select_couplings(fields=["Y1"], output_format="latex")


def test_select_terms_supset_tree6_S():
    assert tree6_jsondb.select_terms(
        fields=["S"], output_format="raw", fields_criterion="supset"
    ) == tree6_sqlitedb.select_terms(
        fields=["S"], output_format="raw", fields_criterion="supset"
    )


def test_select_terms_subset_tree6_Delta1Delta3():
    assert tree6_jsondb.select_terms(
        fields=["Delta1", "Delta3"], output_format="raw", fields_criterion="subset"
    ) == tree6_sqlitedb.select_terms(
        fields=["Delta1", "Delta3"], output_format="raw", fields_criterion="subset"
    )


def test_select_couplings_subset_tree6_Delta1Delta3():
    assert tree6_jsondb.select_couplings(
        fields=["Delta1", "Delta3"], output_format="raw", fields_criterion="subset"
    ) == tree6_sqlitedb.select_couplings(
        fields=["Delta1", "Delta3"], output_format="raw", fields_criterion="subset"
    )
