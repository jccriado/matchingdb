import json
import pathlib

import jsonschema
import pytest
from lepton_loop import lepton_loop_data

from matchingdb.validation import validate


def test_tree6():
    validate(json.loads(pathlib.Path("examples/smeft_dim6_tree.json").read_text()))


def test_lepton_loop_example():
    validate(lepton_loop_data)


def test_random_loop_example():
    validate(lepton_loop_data)


def test_invalid_empty():
    with pytest.raises(jsonschema.ValidationError):
        validate([])


def test_invalid_no_fields():
    with pytest.raises(jsonschema.ValidationError):
        validate(dict(couplings=[], terms=[]))


def test_invalid_coupling_factor():
    with pytest.raises(jsonschema.ValidationError):
        invalid_term = lepton_loop_data["terms"][0].copy()
        invalid_term["factors"][0] = (1, False, 1, ["a", "b"])
        validate(dict(fields=[], couplings=[], terms=[invalid_term]))


def test_invalid_field_order():
    with pytest.raises(jsonschema.ValidationError):
        invalid_term = lepton_loop_data["terms"][0].copy()
        invalid_term["fields"] = ["B", "A"]
        validate(dict(fields=[], couplings=[], terms=[invalid_term]))
