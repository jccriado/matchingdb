Python interface for the [MatchingDB](https://gitlab.com/jccriado/matchingdb) format. 

Install with:
```shell
> pip install git+https://gitlab.com/jccriado/matchingdb.git#subdirectory=python
```

Documentation at [MatchingDB-Python.md](https://gitlab.com/jccriado/matchingdb/-/blob/main/docs/MatchingDB-Python.md).
