[TOC]

# MatchingDB format definition

The format is defined through the [MatchingDB JSON schema](matchingdb_schema.json).
A MatchingDB dictionary is just a JSON value that validates against the schema.
Below, we provide a detailed description of it.

The top-level value must be an object with keys `"fields"`, `"couplings"`, `"constants"` and `"terms"`, whose corresponding values are arrays of objects with the following properties.


## `fields`

Objects in the `"fields"` array represent the heavy fields that have been integrated out:

| Key                | Value type | Description                          |
|--------------------|------------|--------------------------------------|
| `"name"`           | `string`   | Name of the field                    |
| `"real"`           | `boolean`  | Whether the field is real or complex |
| `"representation"` | `string`   | Group representation (free format)   |
| `"latex"`          | `string`   | Latex code to display the field      |


## `couplings`

Objects in the `"couplings"` array represent the couplings in the UV theory:

| Key                   | Value type        | Description                               |
|-----------------------|-------------------|-------------------------------------------|
| `"name"`              | `string`          | Name of the coupling                      |
| `"fields"`            | array of `string` | Heavy fields in the interaction (ordered) |
| `"real"`              | `boolean`         | Whether the field is real or complex      |
| `"latex"`             | `string`          | Latex code to display the coupling        |
| `"latex_interaction"` | `string`          | Latex code to the full interaction term   |


## `constants`

Objects in the `"constants"` array represent the constants that appear in the matching corrections to Wilson coefficients in the EFT. They can be scalars like $`\pi`$ or tensors like $`\delta_{ij}`$:

| Key       | Value type                          | Description                        |
|-----------|-------------------------------------|------------------------------------|
| `"name"`  | `string`                            | Name of the constant               |
| `"value"` | (object of) (array of) ... `number` | See below                          |
| `"latex"` | `string`                            | Latex code to display the constant |

If a constant is complex, its real and imaginary components are stored separately in the `"value"` field as an object as `{"Re": ..., "Im":, ...}`, otherwise it is stored direcly in the value `v`. Tensors are represented as arrays of arrays, with one level of nesting per index of the tensor. 


## `terms`

Objects in the `"terms"` array represent the terms of the matching corrections to each Wilson coefficient in the EFT. Each entry contains information about the coefficient in which the term appears, the heavy fields that generate the term, and data to reconstruct the analytical expression for the term.

| Key              | Value type        | Description                            |
|------------------|-------------------|----------------------------------------|
| `"coefficient"`  | `string`          | Corresponding Wilson coefficient       |
| `"fields"`       | array of `string` | Heavy fields that contribute (ordered) |
| `"factors"`      | array of tuples   | See below                             |
| `"free_indices"` | array of `string` | Must be among the ones in the factors  |

The elements of the `factors` arrays are tuples. That is, they are arrays of fixed length and fixed (inhomogeneous) types for each of their items. The first item of each tuple determines the type of factor it is, among the following:


### `numerical_factor`

The elements of the `factors` array whose first item is `"numerical"` represent a factor $`m^n`$ of some number $`m`$, exponentiated to a power $`n`$, indexed with zero or more indices. The rest of their items must be:

| Item type      | Description                                   |
|----------------|-----------------------------------------------|
| `number`       | Base $`m`$                    |
| `number`       | Exponent $`n`$                                |


### `constant_factor`

The elements of the `factors` array whose first item is `"constant"` represent a factor $`k^n_{ij\ldots}`$ of some constant $`k`$, exponentiated to a power $`n`$, indexed with zero or more indices $`i`$, $`j`$, ... The rest of their items must be:

| Item type      | Description                                   |
|----------------|-----------------------------------------------|
| `string`       | Name of the constant $`k`$                    |
| `number`       | Exponent $`n`$                                |
| array `string` | Indices $`i`$, $`j`$, ...                     |


### `coupling_factor`

The elements of the `factors` array whose first item is `"coupling"` represent a factor $`g^n_{ij\ldots}`$ or $`g^{n*}_{ij\ldots}`$ of some coupling $`g`$, exponentiated to a power $`n`$, indexed with zero or more indices $`i`$, $`j`$, ... The rest of their items must be:

| Item type      | Description                                   |
|----------------|-----------------------------------------------|
| `string`       | Name of the coupling $`g`$                    |
| `number`       | Exponent $`n`$                                |
| `boolean`      | Whether complex conjugation is applied or not |
| array `string` | Indices $`i`$, $`j`$, ...                     |


### `mass_factor`

The elements of the `factors` array whose first item is `"mass"` represent a factor $`M^n_{F,i}`$ of the mass of some field $`F`$, exponentiated to a power $`n`$, with the field's flavor index being $`i`$. The rest of their items must be:

| Item type | Description                     |
|-----------|---------------------------------|
| `string`  | Name of the field $`F`$         |
| `string`  | Flavor index of the field $`i`$ |
| `number`  | Exponent $`n`$                  |
    

### `mass_difference_factor`

The elements of the `factors` array whose first item is `"mass_difference"` represent a factor $`(M^2_{F,i} - M^2_{G,j})^n`$ of the difference between the masses of some fields $`F`$ and $`G`$, with flavor indices $`i`$ and $`j`$, exponentiated to a power $`n`$. The rest of their items must be:

| Item type | Description                           |
|-----------|---------------------------------------|
| `string`  | Name of the first field $`F`$         |
| `string`  | Flavor index of the first field $`i`$ |
| `string`  | Name of the first field $`G`$         |
| `string`  | Flavor index of the first field $`j`$ |
| `number`  | Exponent $`n`$                        |


### `log_mass_factor`

The elements of the `factors` array whose first item is `"log_mass"` represent a factor $`\log\left[ (M_{F,i} / \mu)^n \right]`$ of the logarithm of the ratio between the mass of some field $`F`$ with flavor index $`i`$, and the matching scale $`\mu`$, exponentiated to a power $`n`$. The rest of their items must be:

| Item type | Description                     |
|-----------|---------------------------------|
| `string`  | Name of the field $`F`$         |
| `string`  | Flavor index of the field $`i`$ |
| `number`  | Exponent $`n`$                  |


### `log_mass_ratio_factor`

The elements of the `factors` array whose first item is `"log_mass_ratio"` represent a factor $`\log\left[ (M_{F,i} / (M_{G,j})^n \right]`$ of the logarithm of the ratio between the mass of some field $`F`$ with flavor index $`i`$, and the mass of another field $`G`$ with flavor index $`j`$, exponentiated to a power $`n`$. The rest of their items must be:

| Item type | Description                     |
|-----------|---------------------------------|
| `string`  | Name of the field $`F`$         |
| `string`  | Flavor index of the field $`i`$ |
| `string`  | Name of the field $`G`$         |
| `string`  | Flavor index of the field $`j`$ |
| `number`  | Exponent $`n`$                  |



# SQLite encoding

A MatchingDB can be stored as an SQLite database.
This provides an efficient way to query large dictionaries.

The 3 arrays `"fields"`, `"couplings"`, `"constants"` and `"terms"` are stored as 3 tables with the same names, with each row representing each of the objects in the arrays.
The columns of these tables are named as the keys of the objects.

In each column, the corresponding JSON value is stored directly if its type is `number` or `string`.
Otherwise, a string encoding of it is stored.
