**MatchingDB** is a format for the storage, exchange and exploration of matching dictionaries in EFTs:

* It can represent matching results up to **one-loop** order in full generality.
* It stores information about the **UV theory**, including the heavy fields that have been integrated out and their couplings.
* It allows to easily obtain **bottom-up** and **top-down** information, such as the list of fields and UV couplings that generate a given effective operator, or the effective operators generated by some set of fields and couplings.

This repository contains:

* The [JSON schema](matchingdb.json) that defines the MatchingDB format.
* A [detailed explanation](docs/MatchingDB.md) of the format.
* A Python interface to create, validate, and query MatchingDB dictionaries, located in the [`python`](python/) directory. More information on the interface is provided in the [documentation](docs/MatchingDB-Python.md).
* A collection of pre-computed MatchingDB dictionaries in the [`dictionaries`](dictionaries/) directory. 

An example of a MatchingDB JSON file, with partial 1-loop matching results for a heavy charged lepton is:

``` json
{
  "fields": [
    {"name": "E", "real": false, "representation": "F(1, 1, -1)", "latex": "E"}
  ],
  "couplings": [
    {"name": "lambda_tilde", "fields": ["E"], "real": false, "latex": "\\tilde{\\lambda}", "latex_interaction": ""},
    {"name": "g1", "fields": [], "real": true, "latex": "g_1", "latex_interaction": ""},
    {"name": "g2", "fields": [], "real": true, "latex": "g_2", "latex_interaction": ""},
    {"name": "Ye", "fields": [], "real": false, "latex": "Y_e", "latex_interaction": ""}
  ],
  "constants": [
    {"name": "pi", "value": 3.141592653589793, "latex": "\\pi"},
    {"name": "kdelta", "value": [[1, 0, 0], [0, 1, 0], [0, 0, 1]], "latex": "\\delta"}
  ],
  "terms": [
    {
      "coefficient": "phie",
      "fields": ["E"],
      "factors": [
        ["numerical", 2, 1],
        ["numerical", 240, -1],
        ["constant", "pi", -2, []],
        ["constant", "kdelta", 1, ["i", "j"]],
        ["coupling", "g1", 4, false, []],
        ["mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phie",
      "fields": ["E"],
      "factors": [
        ["numerical", -13, 1],
        ["numerical", 576, -1],
        ["constant", "pi", -2, []],
        ["constant", "kdelta", 1, ["i", "j"]],
        ["coupling", "g1", 2, false, []],
        ["coupling", "lambda_tilde", 1, false, ["k", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["k", "a"]],
        ["mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phie",
      "fields": ["E"],
      "factors": [
        ["numerical", 384, -1],
        ["constant", "pi", -2, []],
        ["coupling", "Ye", 1, true, ["k", "i"]],
        ["coupling", "lambda_tilde", 1, false, ["k", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["l", "a"]],
        ["coupling", "Ye", 1, false, ["l", "j"]], ["mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phie",
      "fields": ["E"],
      "factors": [
        ["numerical", -96, -1],
        ["constant", "pi", -2, []],
        ["constant", "kdelta", 1, ["i", "j"]],
        ["coupling", "g1", 2, false, []],
        ["coupling", "lambda_tilde", 1, false, ["k", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["k", "a"]],
        ["mass", "E", "a", -2], ["log_mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phie",
      "fields": ["E"],
      "factors": [
        ["numerical", 64, -1],
        ["constant", "pi", -2, []],
        ["coupling", "Ye", 1, true, ["k", "i"]],
        ["coupling", "lambda_tilde", 1, false, ["k", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["l", "a"]],
        ["coupling", "Ye", 1, false, ["l", "j"]],
        ["mass", "E", "a", -2],
        ["log_mass", "E", "a", -2]
      ], "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phil3",
      "fields": ["E"],
      "factors": [
        ["numerical", -4, -1],
        ["coupling", "lambda_tilde", 1, false, ["i", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["j", "a"]],
        ["mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phil3",
      "fields": ["E"],
      "factors": [
        ["numerical", -5, 1],
        ["numerical", 1152, -1],
        ["constant", "pi", -2, []],
        ["constant", "kdelta", 1, ["i", "j"]],
        ["coupling", "g2", 2, false, []],
        ["coupling", "lambda_tilde", 1, false, ["k", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["k", "a"]],
        ["mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]
    },
    {
      "coefficient": "phil3",
      "fields": ["E"],
      "factors": [
        ["numerical", -16, -1],
        ["constant", "pi", -2, []],
        ["constant", "kdelta", 1, ["i", "j"]],
        ["coupling", "g2", 2, false, []],
        ["coupling", "lambda_tilde", 1, false, ["k", "a"]],
        ["coupling", "lambda_tilde", 1, true, ["k", "a"]],
        ["mass", "E", "a", -2],
        ["log_mass", "E", "a", -2]
      ],
      "free_indices": ["i", "j"]}
  ]
}

```

This can be validated with any JSON schema validation tool. For example, using [check-jsonschema](https://github.com/python-jsonschema/check-jsonschema):

``` shell
> check-jsonschema --schemafile matchingdb.json E_loop_example.json 
ok -- validation done
```
